jQuery( document ).ready( function ( $ )
{

	// /**
	//  * Split string into multiple values, separated by commas
	//  *
	//  * @param val
	//  *
	//  * @return array
	//  */
	// function split( val )
	// {
	// 	return val.split( /,\s*/ );
	// }
	//
	// /**
	//  * Extract string Last into multiple values
	//  * @param term
	//  *
	//  */
	// function extract_last( term )
	// {
	// 	return split( term ).pop();
	// }
	//
	// $( '#recipient' ).autocomplete( {
	// 	source: function ( request, response )
	// 	{
	// 		var data = {
	// 			action: 'rwpm_get_users',
	// 			term  : extract_last( request.term )
	// 		};
	// 		$.post( ajaxurl, data, function ( r )
	// 		{
	// 			response( r );
	// 		}, 'json' );
	// 	},
	// 	select: function ( event, ui )
	// 	{
	// 		var terms = split( this.value );
	// 		terms.pop();
	// 		terms.push( ui.item.value );
	// 		terms.push( "" );
	// 		this.value = terms.join( "," );
	// 		return false;
	// 	}
	// } );


	$('.manage-column.check-column input[type="checkbox"]').bind('change', function () {

	   if ($(this).is(':checked'))
	     $('.check-column input[type="checkbox"]').prop('checked',true);
	   else
	     $('.check-column input[type="checkbox"]').prop('checked',false);

	});

	// update messages
	function formatList(data){
		var out = '';
		$.each(data['messages'], function( index, value ) {
			out += '<li>'+
							'<a href="'+value['url']+'">'+
									'<span class="photo"><img alt="avatar" src="'+value['userimage']+'"></span>'+
									'<span class="subject">'+
									'<span class="from">'+value['username']+'</span>'+
									'<span class="time"'+value['when']+'</span>'+
									'</span>'+
									'<span class="message">'+
										'<strong>'+value['title']+'</strong>'+
									'</span>'+
							'</a>'+
					'</li>';
		});
		return out;
	}
	function updateMiniInbox(){
		var minb = $( '#header-mini-inbox' );
		$.ajax({
		  method: "POST",
		  url: sitedata.ajaxurl ,
		  data: {
				action: 'rwpm_get_last_messages',
				max: 5,
			},
			dataType: "json",
			})
		  .done(function( data ) {
				//console.log(data);
		    minb.find('.messages-count').text(data['unreaded']);
				if (data['unreaded']){
					minb.find('.head').text('Masz '+data['unreaded']+' wiadomości');
				} else {
					minb.find('.head').text('Brak nowych wiadomości');
				}
				minb.find('.messages').html(formatList(data));
		});
	}
	updateMiniInbox();
	setInterval(updateMiniInbox, 60000);

} );
