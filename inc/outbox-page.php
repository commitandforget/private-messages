<?php
/**
 * Outbox page
 */
function rwpm_outbox()
{
    global $wpdb, $current_user;

    // if view message
    if (isset($_GET['action']) && 'view' == $_GET['action'] && !empty($_GET['id'])) {
        $id = $_GET['id'];

        check_admin_referer("rwpm-view_outbox_msg_$id");

        // select message information
        $msg = $wpdb->get_row('SELECT * FROM ' . $wpdb->prefix . 'pm WHERE `id` = "' . $id . '" LIMIT 1');
        $msg->recipient = $wpdb->get_var("SELECT display_name FROM $wpdb->users WHERE user_login = '$msg->recipient'");
        ?>
        <a href="?page=rwpm_outbox" class="btn btn-link btn-sm"><i class="fa fa-angle-left"></i> <?php _e('Back to outbox', 'pm4wp'); ?></a>
        <h3><i class="fa fa-angle-right"></i> <?php echo stripcslashes($msg->subject); ?></h3>
        <p><?php printf( __( '<b>Sender</b>: %s<br /><b>Date</b>: %s', 'pm4wp' ), $msg->sender, $msg->date ); ?></p>
        <div class="well"><?php echo nl2br( stripcslashes( $msg->content ) ); ?></div>
        <a class="btn btn-danger btn-sm" href="<?php echo wp_nonce_url("?page=rwpm_outbox&action=delete&id=$msg->id", 'rwpm-delete_outbox_msg_' . $msg->id); ?>>"><i class="fa fa-trash-o fa-lg"></i><?php _e( 'Delete', 'pm4wp' ); ?>
        </a>
    <?php
        // don't need to do more!
        return;
    }

    // if delete message
    if (isset($_GET['action']) && 'delete' == $_GET['action'] && !empty($_GET['id'])) {
        $id = $_GET['id'];

        if (!is_array($id)) {
            check_admin_referer("rwpm-delete_outbox_msg_$id");
            $id = array($id);
        } else {
            check_admin_referer("rwpm-bulk-action_outbox");
        }
        $error = false;
        foreach ($id as $msg_id) {
            // check if the recipient has deleted this message
            $recipient_deleted = $wpdb->get_var('SELECT `deleted` FROM ' . $wpdb->prefix . 'pm WHERE `id` = "' . $msg_id . '" LIMIT 1');
            // create corresponding query for deleting message
            if ($recipient_deleted == 2) {
                $query = 'DELETE from ' . $wpdb->prefix . 'pm WHERE `id` = "' . $msg_id . '"';
            } else {
                $query = 'UPDATE ' . $wpdb->prefix . 'pm SET `deleted` = "1" WHERE `id` = "' . $msg_id . '"';
            }

            if (!$wpdb->query($query)) {
                $error = true;
            }
        }
        if ($error) {
            $status = array('content' => __('Error. Please try again.', 'pm4wp'), 'type' => 'danger');
        } else {
            $status = array('content' =>  _n('Message deleted.', 'Messages deleted.', count($id), 'pm4wp'), 'type' => 'success');
        }
    }

    // show all messages
    $msgs = $wpdb->get_results('SELECT `id`, `recipient`, `subject`, `date` FROM ' . $wpdb->prefix . 'pm WHERE `sender` = "' . $current_user->user_login . '" AND `deleted` != 1 ORDER BY `date` DESC');
    ?>
    <h3><i class="fa fa-angle-right"></i> <?php _e('Outbox', 'pm4wp'); ?> <span class="badge"><?php echo count($msgs); ?></span></h3>
    <?php
    if (!empty($status)) {
        echo '<div class="alert alert-'.$status['type'].' alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          ', $status['content'], '</div>';
    }
    if (empty($msgs)) {
        echo '<p>', __('You have no items in outbox.', 'pm4wp'), '</p>';
    } else {
        ?>
        <form action="" method="get">
            <?php wp_nonce_field('rwpm-bulk-action_outbox'); ?>
            <input type="hidden" name="action" value="delete"/> <input type="hidden" name="page" value="rwpm_outbox"/>

            <section id="no-more-tables">
      				<table class="table table-hover table-striped table-condensed cf" cellspacing="0">
                  <thead>
                  <tr>
                      <th class="manage-column check-column" width="3%" ><input type="checkbox"/></th>
                      <th class="manage-column" width="10%"><?php _e('Recipient', 'pm4wp'); ?></th>
                      <th class="manage-column"><?php _e('Subject', 'pm4wp'); ?></th>
                      <th class="manage-column" width="20%"><?php _e('Date', 'pm4wp'); ?></th>
                      <th class="manage-column" width="10%"></th>
                  </tr>
                  </thead>
                  <tbody>
                      <?php
                      foreach ($msgs as $msg) {
                          $msg->recipient = $wpdb->get_var("SELECT display_name FROM $wpdb->users WHERE user_login = '$msg->recipient'");
                          ?>
                      <tr>
                          <td class="check-column"  data-title="Zaznacz" ><input type="checkbox" name="id[]" value="<?php echo $msg->id; ?>"/>
                          </td>
                          <td data-title="<?php _e('Recipient', 'pm4wp'); ?>"><?php echo $msg->recipient; ?></td>
                          <td data-title="<?php _e('Subject', 'pm4wp'); ?>">
                              <?php
                              echo '<a href="', wp_nonce_url("?page=rwpm_outbox&action=view&id=$msg->id", 'rwpm-view_outbox_msg_' . $msg->id), '">', stripcslashes($msg->subject), '</a>';
                              ?>
                          </td>
                          <td data-title="<?php _e('Date', 'pm4wp'); ?>"><?php echo $msg->date; ?></td>
                          <td class="actions-row" >
                            <a class="btn btn-danger btn-xs" href="<?php echo wp_nonce_url("?page=rwpm_outbox&action=delete&id=$msg->id", 'rwpm-delete_outbox_msg_' . $msg->id); ?>"><i class="fa fa-trash-o fa-lg"></i> <?php _e('Delete', 'pm4wp'); ?></a>
                          </td>
                      </tr>
                          <?php

                      }
                      ?>
                  </tbody>
              </table>
          </section>
          <button type="submit" class="btn btn-danger btn-sm" ><i class='fa fa-trash-o fa-lg'></i> <?php _e('Delete Selected', 'pm4wp'); ?></button>
        </form>
        <?php

    }
    ?>
</div>
<?php
}
?>
