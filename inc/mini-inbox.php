<?php
function rwpm_get_mini_inbox($max = 5){
  global $wpdb, $current_user;

  $default_img = "data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMxLjAxMiAzMS4wMTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMxLjAxMiAzMS4wMTI7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNjRweCIgaGVpZ2h0PSI2NHB4Ij4KPGc+Cgk8Zz4KCQk8cGF0aCBkPSJNMjUuMTA5LDIxLjUxYy0wLjEyMywwLTAuMjQ2LTAuMDQ1LTAuMzQyLTAuMTM2bC01Ljc1NC01LjM5OGMtMC4yMDEtMC4xODgtMC4yMTEtMC41MDUtMC4wMjItMC43MDYgICAgYzAuMTg5LTAuMjAzLDAuNTA0LTAuMjEyLDAuNzA3LTAuMDIybDUuNzU0LDUuMzk4YzAuMjAxLDAuMTg4LDAuMjExLDAuNTA1LDAuMDIyLDAuNzA2QzI1LjM3NSwyMS40NTcsMjUuMjQzLDIxLjUxLDI1LjEwOSwyMS41MXogICAgIiBmaWxsPSIjMDAwMDAwIi8+CgkJPHBhdGggZD0iTTUuOTAyLDIxLjUxYy0wLjEzMywwLTAuMjY2LTAuMDUzLTAuMzY1LTAuMTU4Yy0wLjE4OS0wLjIwMS0wLjE3OS0wLjUxOCwwLjAyMi0wLjcwNmw1Ljc1Ni01LjM5OCAgICBjMC4yMDItMC4xODgsMC41MTktMC4xOCwwLjcwNywwLjAyMmMwLjE4OSwwLjIwMSwwLjE3OSwwLjUxOC0wLjAyMiwwLjcwNmwtNS43NTYsNS4zOThDNi4xNDgsMjEuNDY1LDYuMDI1LDIxLjUxLDUuOTAyLDIxLjUxeiIgZmlsbD0iIzAwMDAwMCIvPgoJPC9nPgoJPHBhdGggZD0iTTI4LjUxMiwyNi41MjlIMi41Yy0xLjM3OCwwLTIuNS0xLjEyMS0yLjUtMi41VjYuOTgyYzAtMS4zNzksMS4xMjItMi41LDIuNS0yLjVoMjYuMDEyYzEuMzc4LDAsMi41LDEuMTIxLDIuNSwyLjV2MTcuMDQ3ICAgQzMxLjAxMiwyNS40MDgsMjkuODksMjYuNTI5LDI4LjUxMiwyNi41Mjl6IE0yLjUsNS40ODJjLTAuODI3LDAtMS41LDAuNjczLTEuNSwxLjV2MTcuMDQ3YzAsMC44MjcsMC42NzMsMS41LDEuNSwxLjVoMjYuMDEyICAgYzAuODI3LDAsMS41LTAuNjczLDEuNS0xLjVWNi45ODJjMC0wLjgyNy0wLjY3My0xLjUtMS41LTEuNUgyLjV6IiBmaWxsPSIjMDAwMDAwIi8+Cgk8cGF0aCBkPSJNMTUuNTA2LDE4LjAxOGMtMC42NjUsMC0xLjMzLTAuMjIxLTEuODM2LTAuNjYyTDAuODMsNi4xNTVDMC42MjIsNS45NzQsMC42LDUuNjU4LDAuNzgxLDUuNDQ5ICAgYzAuMTgzLTAuMjA4LDAuNDk4LTAuMjI3LDAuNzA2LTAuMDQ4bDEyLjg0LDExLjJjMC42MzksMC41NTcsMS43MTksMC41NTcsMi4zNTcsMEwyOS41MDgsNS40MTkgICBjMC4yMDctMC4xODEsMC41MjItMC4xNjEsMC43MDYsMC4wNDhjMC4xODEsMC4yMDksMC4xNiwwLjUyNC0wLjA0OCwwLjcwNkwxNy4zNDIsMTcuMzU1ICAgQzE2LjgzNSwxNy43OTcsMTYuMTcxLDE4LjAxOCwxNS41MDYsMTguMDE4eiIgZmlsbD0iIzAwMDAwMCIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=";

  if ( !is_user_logged_in() )
  {
    return;
  }
  $return_array = array(
    'unreaded' => '',
    'messages_count' => '',
    'messages' => array(),
  );
  // get number of PM
  $num_pm = $wpdb->get_var( 'SELECT COUNT(*) FROM ' . $wpdb->prefix . 'pm WHERE `recipient` = "' . $current_user->user_login . '" AND `deleted` != "2"' );
  $num_unread = $wpdb->get_var( 'SELECT COUNT(*) FROM ' . $wpdb->prefix . 'pm WHERE `recipient` = "' . $current_user->user_login . '" AND `read` = 0 AND `deleted` != "2"' );

  if ( empty( $num_pm ) )
  {
    $num_pm = 0;
  }
  if ( empty( $num_unread ) )
  {
    $num_unread = 0;
  }
    $return_array['unreaded'] = $num_unread;
    $return_array['messages_count'] = $num_pm;


  if ( $max )
  {
    $msgs = $wpdb->get_results( 'SELECT `id`, `sender`, `subject`, `read`, `date` FROM ' . $wpdb->prefix . 'pm WHERE `recipient` = "' . $current_user->user_login . '" AND `read` = 0 AND `deleted` != "2" ORDER BY `date` DESC LIMIT ' . $max );
    if ( count( $msgs ) )
    {
      foreach ( $msgs as $msg )
      {
        $msg->sender = $wpdb->get_var( "SELECT display_name FROM $wpdb->users WHERE user_login = '$msg->sender'" );
        $return_array['messages'][] = array(
          'title' => $msg->subject,
          'readed' => $msg->read,
          'username' => $msg->sender,
          'userimage' => $default_img,
          'when' => $msg->date,
          'url' => wp_nonce_url( get_permalink(PM4WP_INBOX_PAGE_ID)."?page=rwpm_inbox&action=view&id=$msg->id", 'rwpm-view_inbox_msg_' . $msg->id ),
        );
      }
    }
  }
  return $return_array;
}
add_action( 'notification_bar', 'rwpm_print_mini_inbox' );

function rwpm_print_mini_inbox(){
  echo '<!-- inbox dropdown start-->
  <li id="header-mini-inbox" class="dropdown">
      <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
          <i class="fa fa-envelope-o"></i>
          <span class="badge bg-theme messages-count"></span>
      </a>
      <ul class="dropdown-menu extended inbox">
        <div class="notify-arrow notify-arrow-green"></div>
        <li class="green"><p class="head">Masz 1 nową wiadomości</p></li>
        <div class="scroll-y messages">

        </div>
        <li><a href="'.get_permalink(PM4WP_INBOX_PAGE_ID).'">Zobacz skrzynkę</a></li>
      </ul>
  </li>
  <!-- inbox dropdown end -->';
}

function rwpm_get_last_messages(){
  $keyword = trim( strip_tags( $_POST['max'] ) );
	$values = rwpm_get_mini_inbox($keyword);
	die( json_encode( $values ) );
}
