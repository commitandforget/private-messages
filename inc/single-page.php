<?php
/*
 * Single page template
 * add <?php do_action( 'main_inbox' ); ?> on template page
 */

//Nav
function rwpm_nav_tabs(){
	?>
	<ul class="nav nav-tabs showback-tabs">
	  <li role="presentation" class="<?php echo ('rwpm_inbox' == $_GET['page'] || !$_GET['page']) ? 'active' : '' ?>"><a href="?page=rwpm_inbox"><?php _e( 'Inbox', 'pm4wp' ); ?></a></li>
	  <li role="presentation" class="<?php echo ('rwpm_outbox' == $_GET['page']) ? 'active' : '' ?>"><a href="?page=rwpm_outbox"><?php _e('Outbox \ View Message', 'pm4wp'); ?></a></li>
	  <li role="presentation" class="<?php echo ('rwpm_send' == $_GET['page']) ? 'active' : '' ?>"><a href="?page=rwpm_send"><?php _e( 'Send Private Message', 'pm4wp' ); ?></a></li>
	</ul>
	<?php
}

function rwpm_main_wrapper(){
  echo '<div class="showback">';
	switch ($_GET['page']) {
		case 'rwpm_outbox':
			rwpm_outbox();
			break;
		case 'rwpm_send':
			rwpm_send();
			break;
		default:
			rwpm_inbox();
			break;
	}
  echo '</div>';
}

add_action( 'main_inbox', 'rwpm_nav_tabs' );
add_action( 'main_inbox', 'rwpm_main_wrapper' );
